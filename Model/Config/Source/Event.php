<?php

namespace BelVG\Halloween\Model\Config\Source;

class Event implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'blackfriday', 'label' => 'Black Friday'],
            ['value' => 'cyberday', 'label' => 'Cyber Monday'],
            ['value' => 'easterday', 'label' => 'Easter'],
            ['value' => 'halloween', 'label' => 'Halloween'],
            ['value' => 'independenceday', 'label' => 'Independence Day'],
            ['value' => 'laborday', 'label' => 'Labor Day'],
            ['value' => 'thanksgivingday', 'label' => 'Thanksgiving'],
            ['value' => 'xmas-newyear', 'label' => 'Christmas & New Year'],
            ['value' => 'newyear', 'label' => 'New Year v1'],
            ['value' => 'newyear2', 'label' => 'New Year v2'],
            ['value' => 'xmas', 'label' => 'Christmas v1'],
            ['value' => 'xmas2', 'label' => 'Christmas v2'],
            ['value' => 'patricksday', 'label' => 'Saint Patrick\'s Day'],
            ['value' => 'valentinesday', 'label' => 'Valentine\'s Day v1'],
            ['value' => 'valentinesday2', 'label' => 'Valentine\'s Day v2'],
        ];
    }
}