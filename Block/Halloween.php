<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   Belvg
 * @package    BelVG_Halloween
 * @copyright  Copyright (c) 2010 - 2016 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

namespace BelVG\Halloween\Block;

/**
 * Class Halloween
 * @package BelVG\Halloween\Block
 */
class Halloween extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \BelVG\Halloween\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $_productImageHelper;

    const IMAGE_SIZE = 60;

    /**
     * Halloween constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \BelVG\Halloween\Helper\Data $dataHelper
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Helper\Image $productImageHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \BelVG\Halloween\Helper\Data $dataHelper,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Helper\Image $productImageHelper,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productImageHelper = $productImageHelper;
        $this->dataHelper = $dataHelper;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->dataHelper->isAllowed();
    }

    /**
     * Get block position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->dataHelper->getPosition();
    }

    /**
     * Block event type
     *
     * @return string
     */
    public function getEventType()
    {
        return $this->dataHelper->getEventType();
    }

    /**
     * Get cooke nme for disabling block output
     *
     * @retur string;
     */
    public function getCookieName()
    {
        return $this->dataHelper->getCookieName();
    }

    /**
     * Return expiry date of the cookie for hiding banner
     *
     * @return string
     */
    public function getExpires()
    {
        return $this->dataHelper->getCookieExpires();
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getProductCollection()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*')
            ->addAttributeToFilter('sku', ['in' => $this->dataHelper->getSkus()])
            ->load();
        return $collection;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getImage(\Magento\Catalog\Model\Product $product)
    {
        return $this->_productImageHelper
            ->init($product, 'product_thumbnail_image')
            ->constrainOnly(false)
            ->keepAspectRatio(true)
            ->keepFrame(false)
            ->resize(self::IMAGE_SIZE)
            ->getUrl();
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getProductPrice(\Magento\Catalog\Model\Product $product)
    {
        $priceRender = $this->getPriceRender();

        $price = '';
        if ($priceRender) {
            $price = $priceRender->render(
                \Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE,
                $product,
                [
                    'include_container' => true,
                    'display_minimal_price' => true,
                    'zone' => \Magento\Framework\Pricing\Render::ZONE_ITEM_LIST,
                    'list_category_page' => true
                ]
            );
        }

        return $price;
    }

    /**
     * @return \Magento\Framework\Pricing\Render
     */
    protected function getPriceRender()
    {
        return $this->getLayout()->getBlock('product.price.render.default');
    }
}
