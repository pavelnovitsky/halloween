<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   Belvg
 * @package    BelVG_Halloween
 * @copyright  Copyright (c) 2010 - 2016 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

namespace BelVG\Halloween\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config;

/**
 * Class Data
 * @package BelVG\Halloween\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * is block active
     */
    const XML_PATH_HALLOWEEN_ENABLED = 'halloween/general/enable';

    /**
     * Block position
     */
    const XML_PATH_HALLOWEEN_POSITION = 'halloween/general/position';

    /**
     * Block event type
     */
    const XML_PATH_HALLOWEEN_EVENT = 'halloween/general/event';

    /**
     * List of selected SKUs
     */
    const XML_PATH_HALLOWEEN_SKUS = 'halloween/general/products';

    /**
     * Module cache tag
     */
    const CACHE_TAG = 'belvg_halloween';

    /**
     * cookie for disabling block output
     */
    const COOKIE_NAME = 'promo-adv';

    /**
     * Separator for the selected SKUs
     *
     * @var string
     */
    protected $skus_separator = ',';

    /**
     * @var Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $cookieManager = null;

    /**
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->cookieManager = $objectManager->get('Magento\Framework\Stdlib\CookieManagerInterface');

        parent::__construct($context);
    }

    /**
     * Check if module is enabled
     * @param string $scopeType
     * @param null|mixed $scopeCode
     * @return bool
     */
    public function isActive($scopeType = Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null)
    {
        return $this->scopeConfig->isSetFlag(self::XML_PATH_HALLOWEEN_ENABLED, $scopeType, $scopeCode);
    }

    /**
     * @param string $scopeType
     * @param null $scopeCode
     * @return mixed
     */
    public function getPosition($scopeType = Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_HALLOWEEN_POSITION, $scopeType, $scopeCode);
    }

    /**
     * @param string $scopeType
     * @param null $scopeCode
     * @return mixed
     */
    public function getEventType($scopeType = Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_HALLOWEEN_EVENT, $scopeType, $scopeCode);
    }

    /**
     * @param string $scopeType
     * @param null $scopeCode
     * @return mixed
     */
    public function getCookieName($scopeType = Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null)
    {
        return $this->scopeConfig->getValue(self::COOKIE_NAME, $scopeType, $scopeCode);
    }

    /**
     * @param string $scopeType
     * @param null $scopeCode
     * @return mixed
     */
    public function getCookie($scopeType = Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null)
    {
        $cookieName = $this->getCookieName($scopeType, $scopeCode);
        return $this->cookieManager->getCookie($cookieName);
    }

    /**
     * @param string $scopeType
     * @param null $scopeCode
     * @return array
     */
    public function getSkus($scopeType = Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null)
    {
        $skuList = $this->scopeConfig->getValue(self::XML_PATH_HALLOWEEN_SKUS, $scopeType, $scopeCode);
        // array of SKUs
        $skuArray = explode($this->skus_separator, $skuList);

        // remove leading spaces from SKUs, remove current product from list
        foreach ($skuArray as $k => $v) {
            $skuArray[$k] = trim($v);
        }

        return $skuArray;
    }

    /**
     * Check if block is allowed to show
     * @param string $scopeType
     * @param null $scopeCode
     * @return bool
     */
    public function isAllowed($scopeType = Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null)
    {
        if (!$this->isActive($scopeType, $scopeCode) || !is_null($this->getCookie($scopeType, $scopeCode))) {
            return false;
        }

        return true;
    }

    /**
     * Return expiry date of the cookie for hiding banner
     *
     * @return string
     */
    public function getCookieExpires()
    {
        return date('r', strtotime('+2 days'));
    }
}
